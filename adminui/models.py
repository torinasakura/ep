from django.db import models

# Create your models here.
class Bank(models.Model):
    active = models.BooleanField(verbose_name=u'Активно')
    bankname = models.CharField(max_length=500, verbose_name=u'Наименование Банка')
    bankaddress = models.CharField(max_length=500, verbose_name=u'Адрес Банка')
    recipientname = models.CharField(max_length=500, verbose_name=u'Имя получателя')
    recipientaddress = models.CharField(max_length=500, verbose_name=u'Адрес получателя')
    number = models.CharField(max_length=500, verbose_name=u'Номер счета')
    bic = models.CharField(max_length=500, verbose_name=u'BIC')
    swift = models.CharField(max_length=500, verbose_name=u'SWIFT')

    def __unicode__(self):
        return self.bankname

    class Meta:
        verbose_name = u"Банковский платеж"
        verbose_name_plural = u"Банковские платежи"


class Purse(models.Model):
    active = models.BooleanField(verbose_name=u'Активно')
    name = models.CharField(max_length=500, verbose_name=u'Название')
    number = models.CharField(max_length=500, verbose_name=u'Номер счета')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Платежная система"
        verbose_name_plural = u"Платежные системы"
