from datetime import datetime, timedelta
from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from django.contrib.auth.decorators import permission_required, login_required
from django.views.decorators.csrf import csrf_exempt
from index.models import Game, Ticket, Series
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from PIL import Image
from models import Bank, Purse
from django.http import HttpResponse
from index.models import Ticket
from cp.models import Invoicep, Invoiceb, Message, Userinfo, Wreq, Profit, Bonus
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


def handle_uploaded_file(f, name):
    destination = open('/tmp/' + name, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    return name


@permission_required('fifer', login_url='/reg/')
def withdrawal(request):
    w = Wreq.objects.all()
    context = {'title': _(u'| Вывод средств'),
               'wreq': w}
    return render(request, 'adminui/withdrawal.html', context)


@permission_required('fifer', login_url='/reg/')
def tickets(request):
    t = Ticket.objects.all()
    context = {'title': _(u'| Билеты'),
               'tikets': t}
    return render(request, 'adminui/tickets.html', context)


@permission_required('fifer', login_url='/reg/')
def banks(request):
    b = Bank.objects.all()
    context = {'title': _(u'| Банковские реквизиты'),
               'banks': b}
    return render(request, 'adminui/banks.html', context)


@permission_required('fifer', login_url='/reg/')
def purses(request):
    p = Purse.objects.all()
    context = {'title': _(u'| Платежные системы'),
               'purses': p}
    return render(request, 'adminui/purses.html', context)


@permission_required('fifer', login_url='/reg/')
def rembank(request, id):
    b = get_object_or_404(Bank, id=id)
    b.delete()
    return HttpResponse(1)


@permission_required('fifer', login_url='/reg/')
def offbank(request, id):
    b = get_object_or_404(Bank, id=id)
    b.active = False
    b.save()
    return HttpResponse(1)


@permission_required('fifer', login_url='/reg/')
def onbank(request, id):
    b = get_object_or_404(Bank, id=id)
    b.active = True
    b.save()
    return HttpResponse(1)


@permission_required('fifer', login_url='/reg/')
def rempurse(request, id):
    p = get_object_or_404(Purse, id=id)
    p.delete()
    return HttpResponse(1)


@permission_required('fifer', login_url='/reg/')
def offpurse(request, id):
    p = get_object_or_404(Purse, id=id)
    p.active = False
    p.save()
    return HttpResponse(1)


@permission_required('fifer', login_url='/reg/')
def onpurse(request, id):
    p = get_object_or_404(Purse, id=id)
    p.active = True
    p.save()
    return HttpResponse(1)


@permission_required('fifer', login_url='/reg/')
def remticket(request, id):
    t = get_object_or_404(Ticket, id=id)
    t.delete()
    g = t.game
    g.ticketsa = g.ticketsa + 1
    g.save()
    return HttpResponse(1)


@permission_required('fifer', login_url='/reg/')
def editticket(request, id):
    t = get_object_or_404(Ticket, id=id)
    context = {'title': _(u'| Билет'),
               'ticket': t}
    if request.method == "POST":
        status = request.POST['status']
        bdate = datetime.strptime(request.POST['date'], '%d/%m/%Y %H:%M')
        if status == '4':
            u5 = t.user
            u4 = u5.bonus_set.all()[0].refer
            tp = float(t.price)
            rprof = u4.partners
            rprof.bal = float(rprof.bal) + ((tp / 100) * 11)
            rprof.save()
            p = Profit.objects.get_or_create(user=u5, refer=u4)[0]
            p.profit = float(p.profit) + ((tp / 100) * 11)
            p.save()
            bonus = Bonus.objects.filter(user=u4)
            if bonus.count() > 0:
                u3 = bonus[0].refer
                rprof = u3.partners
                rprof.bal = float(rprof.bal) + (tp / 100 * 1)
                rprof.save()
                p = Profit.objects.get_or_create(user=u5, refer=u3)[0]
                p.profit = float(p.profit) + (tp / 100 * 1)
                p.save()
                bonus = Bonus.objects.filter(user=u3)
                if bonus.count() > 0:
                    u2 = bonus[0].refer
                    rprof = u2.partners
                    rprof.bal = float(rprof.bal) + (tp / 100 * 1)
                    rprof.save()
                    p = Profit.objects.get_or_create(user=u5, refer=u2)[0]
                    p.profit = float(p.profit) + (tp / 100 * 1)
                    p.save()
                    bonus = Bonus.objects.filter(user=u2)
                    if bonus.count() > 0:
                        u1 = bonus[0].refer
                        rprof = u1.partners
                        rprof.bal = float(rprof.bal) + (tp / 100 * 1)
                        rprof.save()
                        p = Profit.objects.get_or_create(user=u5, refer=u1)[0]
                        p.profit = float(p.profit) + (tp / 100 * 1)
                        p.save()
                        bonus = Bonus.objects.filter(user=u1)
                        if bonus.count() > 0:
                            u0 = bonus[0].refer
                            rprof = u0.partners
                            rprof.bal = float(rprof.bal) + (tp / 100 * 1)
                            rprof.save()
                            p = Profit.objects.get_or_create(user=u5, refer=u0)[0]
                            p.profit = float(p.profit) + (tp / 100 * 1)
                            p.save()
            try:
                b = t.invoiceb
                b.status = 1
                b.save()
            except:
                p = t.invoicep
                p.status = 1
                p.save()
            m = Message()
            m.user = t.user
            m.read = False
            m.text = _(u'Ваш билет %(game)s игра № %(gamen)s билет № %(tnum)s подтвержден администратором') % {
                'game': _(t.game.series.type.name), 'gamen': str(t.game.num), 'tnum': str(t.numbert)}
            m.title = _(u'Ваша заявка была подтверждена администратором')
            m.save()
        if status == '5':
            m = Message()
            m.user = t.user
            m.read = False
            m.text = _(u'Ваш билет %(game)s игра № %(gamen)s билет № %(tnum)s отклонен администратором') % {
                'game': _(t.game.series.type.name), 'gamen': str(t.game.num), 'tnum': str(t.numbert)}
            m.title = _(u'Ваша заявка была отклонена администратором')
            m.save()
            g = t.game
            g.ticketsa = g.ticketsa + 1
            g.save()
            t.delete()
            return redirect('/adminui/tickets/')
        t.status = status
        t.bdate = bdate
        t.save()
        return redirect('/adminui/tickets/')
    return render(request, 'adminui/editticket.html', context)


@permission_required('fifer', login_url='/reg/')
def editpurse(request, id):
    p = get_object_or_404(Purse, id=id)
    context = {'title': _(u'| Платежные системы'),
               'p': p}
    if request.method == "POST":
        name = request.POST['name']
        number = request.POST['number']
        p.name = name
        p.number = number
        p.save()
        return redirect('/adminui/purses/')
    return render(request, 'adminui/editpurse.html', context)


@permission_required('fifer', login_url='/reg/')
def editbank(request, id):
    b = get_object_or_404(Bank, id=id)
    context = {'title': '| Платежные системы',
               'b': b}
    if request.method == "POST":
        bankname = request.POST['bankname']
        bankaddress = request.POST['bankaddress']
        recipientname = request.POST['recipientname']
        recipientaddress = request.POST['recipientaddress']
        number = request.POST['number']
        bic = request.POST['bic']
        swift = request.POST['swift']
        b.bankname = bankname
        b.bankaddress = bankaddress
        b.recipientname = recipientname
        b.recipientaddress = recipientaddress
        b.number = number
        b.bic = bic
        b.swift = swift
        b.save()
        return redirect('/adminui/banks/')
    return render(request, 'adminui/editbank.html', context)


@permission_required('fifer', login_url='/reg/')
def addbank(request):
    imgroot = '/var/www/ep/st/p/b/'
    if request.method == "POST":
        bankname = request.POST['bankname']
        bankaddress = request.POST['bankaddress']
        recipientname = request.POST['recipientname']
        recipientaddress = request.POST['recipientaddress']
        number = request.POST['number']
        bic = request.POST['bic']
        swift = request.POST['swift']
        b = Bank()
        b.active = True
        b.bankname = bankname
        b.bankaddress = bankaddress
        b.recipientname = recipientname
        b.recipientaddress = recipientaddress
        b.number = number
        b.bic = bic
        b.swift = swift
        b.save()
        tmpfile = handle_uploaded_file(request.FILES['logo'], str(b.id) + request.FILES['logo'].name[-4:])
        img = Image.open('/tmp/' + tmpfile)
        img.save(imgroot + str(b.id) + '.png', 'PNG')
        request.session['alert'] = _(u'Банковские реквизиты успешно добавленны')
        return redirect('/adminui/banks/')


@permission_required('fifer', login_url='/reg/')
def addpurse(request):
    imgroot = '/var/www/ep/st/p/p/'
    if request.method == "POST":
        name = request.POST['name']
        number = request.POST['number']
        p = Purse()
        p.active = True
        p.name = name
        p.number = number
        p.save()
        tmpfile = handle_uploaded_file(request.FILES['logo'], str(p.id) + request.FILES['logo'].name[-4:])
        img = Image.open('/tmp/' + tmpfile)
        img.save(imgroot + str(p.id) + '.png', 'PNG')
        request.session['alert'] = _(u'Платежная система успешно добавленна')
        return redirect('/adminui/purses/')


@permission_required('fifer', login_url='/reg/')
def editwithdrawal(request, id):
    w = get_object_or_404(Wreq, id=id)
    context = {'title': _(u'| Платежные системы'),
               'wreq': w}
    if request.method == "POST":
        status = request.POST['status']
        if status == '0':
            w.status = 0
            w.save()
        if status == '1':
            w.status = 1
            w.save()
        if status == '2':
            w.status = 2
            w.save()
            ui = w.user.partners_set
            ui.bal = ui.bal + w.points
            ui.save()
        return redirect('/adminui/withdrawal/')
    return render(request, 'adminui/editwithdrawal.html', context)


@permission_required('fifer', login_url='/reg/')
def bonus(request):
    b = Bonus.objects.filter(refer=1)
    context = {'bonuss': b}
    return render(request, 'adminui/bonus.html', context)


@permission_required('fifer', login_url='/reg/')
def userinfo(request, id=1):
    u = get_object_or_404(User, id=id)
    t = Ticket.objects.filter(user=u)
    ip = Invoicep.objects.filter(user=u)
    ib = Invoiceb.objects.filter(user=u)
    p = Bonus.objects.filter(refer=u)
    users = []
    for item in p:
        users.append(item.user)
    referals = []
    for item in p:
        tickets = Ticket.objects.filter(user=item.user, status__in=[2, 3, 4])
        sum = 0
        for ticket in tickets:
            sum += ticket.price
        referals.append({'user': item.user, 'sum': sum})
    sp = Profit.objects.filter(refer=u).exclude(user__in=users)
    sreferals = []
    for item in sp:
        tickets = Ticket.objects.filter(user=item.user, status__in=[2, 3, 4])
        sum = 0
        for ticket in tickets:
            sum += ticket.price
        sreferals.append({'user': item.user, 'sum': sum})
    context = {'user': u,
               'tickets': t,
               'invoicep': ip,
               'invoiceb': ib,
               'referals': referals,
               'sreferals': sreferals}
    return render(request, 'adminui/userinfo.html', context)