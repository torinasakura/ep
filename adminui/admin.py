from django.contrib import admin
from models import Purse, Bank


admin.site.register(Purse)
admin.site.register(Bank)