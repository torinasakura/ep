from django.conf.urls import patterns, url

urlpatterns = patterns('',
                       url(r'^$', 'adminui.views.tickets'),
                       url(r'^tickets/', 'adminui.views.tickets'),
                       url(r'^banks/', 'adminui.views.banks'),
                       url(r'^purses/', 'adminui.views.purses'),
                       url(r'^withdrawal/', 'adminui.views.withdrawal'),
                       url(r'^addbank/', 'adminui.views.addbank'),
                       url(r'^addpurse/', 'adminui.views.addpurse'),
                       url(r'^rembank/(?P<id>\d+)/', 'adminui.views.rembank'),
                       url(r'^offbank/(?P<id>\d+)/', 'adminui.views.offbank'),
                       url(r'^onbank/(?P<id>\d+)/', 'adminui.views.onbank'),
                       url(r'^rempurse/(?P<id>\d+)/', 'adminui.views.rempurse'),
                       url(r'^offpurse/(?P<id>\d+)/', 'adminui.views.offpurse'),
                       url(r'^onpurse/(?P<id>\d+)/', 'adminui.views.onpurse'),
                       url(r'^editbank/(?P<id>\d+)/', 'adminui.views.editbank'),
                       url(r'^editpurse/(?P<id>\d+)/', 'adminui.views.editpurse'),
                       url(r'^remticket/(?P<id>\d+)/', 'adminui.views.remticket'),
                       url(r'^editticket/(?P<id>\d+)/', 'adminui.views.editticket'),
                       url(r'^editwithdrawal/(?P<id>\d+)/', 'adminui.views.editwithdrawal'),
                       url(r'^bonus/', 'adminui.views.bonus'),
                       url(r'^userinfo/(?P<id>\d+)/', 'adminui.views.userinfo'),


)
