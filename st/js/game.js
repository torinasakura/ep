jQuery(document).ready(function($){

    var freeTickets;//Array
    /*----------------------------------------------------*/
    /*	Data Table
    /*----------------------------------------------------*/
    $('#tickets').dataTable();

    /*----------------------------------------------------*/
    /*	Messenger Options
    /*----------------------------------------------------*/
    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-center',
        theme: 'flat'
    }

    /*----------------------------------------------------*/
    /*	Spinner
    /*----------------------------------------------------*/
    var game = $('[data-tickets]').data('tickets'),
        ticketSize = Number(game.tickets) - 1,
        series = ticketSize.toString().charAt(0),
        tickets = ticketSize.toString().substr(1),
        ticketsLength = tickets.length,
        startTicketsValue = '';

    for (var j = 0; j < ticketsLength; j++){
        startTicketsValue += '0';
    }

    $("input[name='addserial']").TouchSpin({
        min: 0,
        max: series,
        initval: 0,
        postfix: gettext('Случайная серия'),
        postfix_extraclass: "btn btn-default random-serial"
    });
    $("input[name='addtickets']").TouchSpin({
        min: 0,
        max: tickets,
        initval: 0,
        postfix: gettext('Случайный билет'),
        postfix_extraclass: "btn btn-default random-tickets"
    });

    $('#addtickets').val(startTicketsValue);

    /*----------------------------------------------------*/
    /*	Random Serial
    /*----------------------------------------------------*/
    function randomSerial(){
        if(freeTickets){
            var freeTicketsNumber = freeTickets.length,
                random = 0 + Math.floor(Math.random() * freeTicketsNumber),
                randomSerialNum = freeTickets[random].charAt(0);

            $('#addserial').val(randomSerialNum);
            $('#confirm_ticket').prop("disabled", false);
        }
    }


    $('.random-serial').click(function(){
        randomSerial();
    })

    /*----------------------------------------------------*/
    /*	Random Tickets
    /*----------------------------------------------------*/

    function randomTickets(){
        var series = $('#addserial').val(),
            tickets, random, randomTicketNum, freeTicketsNumber,
            container = '';
        if(freeTickets){
            for(var i = 0,ser; i < freeTickets.length; i++){
                ser = freeTickets[i].charAt(0);
                if(series == ser){
                    container += ',' + freeTickets[i].substr(1);
                }
            }

            if(container.length != 0){
                container = container.substr(1);
                tickets = container.split(',');
                freeTicketsNumber = tickets.length;
                random = 0 + Math.floor(Math.random() * freeTicketsNumber);
                randomTicketNum = tickets[random];
                $('#addtickets').val(randomTicketNum);
                $('#confirm_ticket').prop("disabled", false);
            }
        }
    }

    $('.random-tickets').click(function(){
        randomTickets();
    })

    /*----------------------------------------------------*/
    /*	Check ticket setInteval
    /*----------------------------------------------------*/

    $('#addserial, #addtickets').change(function(){
        var game = $('#buy_ticket').data('game-id'),
            series = $('#addserial').val(),
            tickets = $('#addtickets').val().toString(),
            ticketsLength = $('#game_item_'+game).data('tickets').tickets.length - 1,
            difference = ticketsLength - tickets.length,
            addedValue = '';

        for (var j = 0; j < difference; j++){
            addedValue += '0';
        }
        tickets = addedValue + tickets;
        //Zero padding the ticket number
        $('#addtickets').val(tickets);

        if(freeTickets){
            var ticketNum = series + tickets;
            checkTickets = $.inArray(ticketNum, freeTickets);

            if(checkTickets == -1){
                var msg =  Messenger().post({
                    message: gettext('Билет с номером '+ ticketNum+' уже занят'),
                    type: 'error',
                    showCloseButton: true,
                    hideAfter: 4,
                    hideOnNavigate: true
                });
                $('#confirm_ticket').prop("disabled", true);
            }
            else{
                $('#confirm_ticket').prop("disabled", false);
            }
        }
    })

    /*----------------------------------------------------*/
    /*	Check ticket
    /*----------------------------------------------------*/
    function checkTicket(ticketNum,gameNum,callback){
        var url = '/checkticket/';

        $.ajax({
            type: "GET",
            url: url,
            data:{game:gameNum,ticket:ticketNum},
            dataType: 'json',
            success: function (data) {
                if(data == 1){
                    $('#confirm_ticket').prop("disabled", false);

                    if (callback && typeof(callback) === "function") {
                        callback();
                    }
                }
                else if(data == 0){
                    Messenger().post({
                        message: gettext('Билет с номером '+ ticketNum+' уже занят'),
                        type: 'error',
                        showCloseButton: true,
                        hideAfter: 4,
                        hideOnNavigate: true
                    });
                    $('#confirm_ticket').prop("disabled", true);
                }
                return false;
            }
        });
    }


    /*----------------------------------------------------*/
    /*	getCookie
    /*----------------------------------------------------*/
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    /*----------------------------------------------------*/
    /*	Buy ticket
    /*----------------------------------------------------*/
    function buyTicket(ticket,gameNum){
        var url = '/buyticket/',
            csrftoken = getCookie('csrftoken');

        $.ajax({
            type: "POST",
            url: url,
            data:{game:gameNum,ticket:ticket,csrfmiddlewaretoken:csrftoken},
            dataType: 'json',
            success: function (data) {
                var date = data.date.toString().substring(0,16);
                $('#tickets').find('tbody').prepend('<tr class="table-ticket" data-ticket-id="'+data.idTicket+'"}>' +
                    '<td><a href="#">'+data.numTicket+'</a></td>'+
                    '<td>Игра '+data.numGame+'</td>'+
                    '<td> '+data.typeGame+' </td>'+
                    '<td><i class="icon-euro"> </i>'+data.price+'</td>'+
                    '<td><span class="label label-sm label-primary">'+gettext('Забронирован до ')+date+'</span></td>'+
                    '<td><div class="visible-md visible-lg hidden-sm hidden-xs btn-group">' +
                    '<button class="btn btn-xs  btn-success payments-ticket">'+gettext('Оплатить')+'<i class="icon-ok bigger-120"></i></button>'+
                    '<button class="btn btn-xs cancel-ticket btn-danger cancel-ticket">'+gettext('Аннулировать')+'<i class="icon-trash bigger-120"></i>'+
                    '</div></td>'+
                    '</tr>');

                $('#tickets').parents('.row').show();
                gameProgressTickets();

                Messenger().post({
                    message: gettext('Выбранный вами номер подтвержден. Номер вашего билета: ')+ ticket,
                    type: 'success',
                    showCloseButton: true,
                    hideAfter: 4,
                    hideOnNavigate: true,
                    off: function(){
                        alert('close')
                    }
                });
                $('#buy_ticket').removeData('game-id');
                $('#buy_ticket').modal('hide');
            }
        });
    }

    /*----------------------------------------------------*/
    /*	Confirm ticket
    /*----------------------------------------------------*/
    var confirmInterval = (function(){
        var lock = false;

        return function(){
            if(lock){
                clearTimeout(lock);
            }

            lock = setTimeout(function(){
                var game = $('#buy_ticket').data('game-id'),
                    series = $('#addserial').val(),
                    tickets = $('#addtickets').val().toString(),
                    ticketsLength = $('#game_item_'+game).data('tickets').ticketsa.length - 1,
                    difference = ticketsLength - tickets.length,
                    addedValue = '';

                for (var j = 0; j < difference; j++){
                    addedValue += '0';
                }
                tickets = addedValue + tickets;

                var ticketNum = series + tickets;
                checkTicket(ticketNum,game,function(){
                    buyTicket(ticketNum,game);
                });

            }, 500)
        }
    }());

    $('#confirm_ticket').click(function(){
        confirmInterval();
    });


    /*----------------------------------------------------*/
    /*	Modal Game
    /*----------------------------------------------------*/
    $(document).on('click','button.buy-ticket',function(){
        var game = $(this).parents('[data-tickets]').data('tickets'),
            ticketsLength = game.tickets.length,
            url = '/ftickets/'+game.id+'/';

        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function (data) {
                freeTickets = data;
                for(var i = 0; i < freeTickets.length; i++){
                    freeTickets[i] = freeTickets[i].toString();
                    if(freeTickets[i].length < ticketsLength){
                        var difference = ticketsLength - freeTickets[i].length,
                            addedValue = '';
                        for (var j = 0; j < difference; j++){
                            addedValue += '0';
                        }
                        freeTickets[i] = addedValue + freeTickets[i];
                    }
                }
            }
        });

        $('#buy_ticket').data('game-id',game.id);
        $('#buy_ticket').modal('show');
    })

    /*----------------------------------------------------*/
    /*	Game Progress-bar
    /*----------------------------------------------------*/
    function gameProgressTickets(){
        $('.game-active').each(function(){
            var game = $(this).data('tickets'),
                progress = 100 - (game.ticketsa / (game.tickets * 0.01));
            $(this).find('.progress-bar').css({width:progress+'%'}).data('progress',progress);
        })
    }
    gameProgressTickets();

    /*----------------------------------------------------*/
    /*	Change Game Class
    /*----------------------------------------------------*/
    function changeGame(gameId,status,ticketsa){
        var game = $('#game_item_'+gameId),
            elementClass = '',
            progressBarClass = '',
            gameData = game.data('tickets');

        if(!game){
            return false;
        }

        if(gameData.status != status){
            if(gameData.status == 0){
                progressBarClass = 'progress-bar-default';
            }
            else if(gameData.status == 1){
                elementClass = 'started';
                progressBarClass = 'progress-bar-danger';
            }
            else if(gameData.status == 2){
                elementClass = 'end';
                progressBarClass = 'progress-bar-success';
            }

            if(status == 0){
                game.removeClass('game-active');
                game.find('.vuzz-pricing').removeClass(elementClass);
                game.find('.vuzz-pricing-header').removeClass(elementClass);
                game.find('.vuzz-pricing-button').removeClass(elementClass);
                game.find('.vuzz-pricing-cost').html('<i class="icon-refresh icon-spin"></i>'+gettext('Ожидание'));
                game.find('.progress-bar').removeClass(progressBarClass).addClass('progress-bar-inverse');
                game.find('.buy-ticket').prop("disabled", true).html(gettext('Купить билет'));
                game.data('tickets').status = 0;
            }
            else if(status == 1){
                game.addClass('game-active');
                game.find('.vuzz-pricing').removeClass(elementClass).addClass('started');
                game.find('.vuzz-pricing-header').removeClass(elementClass).addClass('started');
                game.find('.vuzz-pricing-button').removeClass(elementClass).addClass('started');
                game.find('.vuzz-pricing-cost').html('<i class="icon-play-sign"></i>'+gettext('В продаже'));
                game.find('.progress-bar').removeClass(progressBarClass).addClass('progress-bar-danger');
                game.find('.buy-ticket').prop("disabled", false).html(gettext('Купить билет'));
                game.data('tickets').status = 1;
            }
            else if(status == 2){
                game.removeClass('game-active');
                game.find('.vuzz-pricing').removeClass(elementClass).addClass('end');
                game.find('.vuzz-pricing-header').removeClass(elementClass).addClass('end');
                game.find('.vuzz-pricing-button').removeClass(elementClass).addClass('end');
                game.find('.vuzz-pricing-cost').html('<i class="icon-bookmark"></i>'+gettext('Закончена'));
                game.find('.progress-bar').removeClass(progressBarClass).addClass('progress-bar-success');
                game.find('.buy-ticket').prop("disabled", true).html(gettext('Дата розыгрыша: ')+gameData.stopDate);
                game.data('tickets').status = 2;
            }
        }
        if(gameData.tickets != ticketsa){
            var progress = 100 - Math.round(ticketsa / (gameData.tickets * 0.01));
            game.data('tickets').ticketsa = ticketsa;
            game.find('.game-ticketsa').html(gettext('Осталось билетов ')+ticketsa);
            game.find('.progress-bar').css({width:progress+'%'}).data('progress',progress);
        }
    }

    /*----------------------------------------------------*/
    /*	Checked Game Data
    /*----------------------------------------------------*/
     function checkGameData(){
         setInterval(function(){
             $('.game-item').each(function(){
                 var game = $(this).data('tickets'),
                     url = '/checkgame/'+game.id+'/';

                 $.ajax({
                     type: "GET",
                     url: url,
                     dataType: 'json',
                     success: function (data) {
                        changeGame(game.id,data.status,data.ticketsa);
                     }
                 });
             })
         },30000)
     }
     checkGameData();

    /*----------------------------------------------------*/
    /*	Cancel Ticket
    /*----------------------------------------------------*/
    $(document).on('click','.cancel-ticket',function(){
        var ticketId = $(this).parents('.table-ticket').data('ticket-id'),
            csrftoken = getCookie('csrftoken'),
            url = '/cp/cancelticket/';

        var msg = Messenger().post({
            message: gettext('Вы уверены, что хотите аннулировать свой билет?'),
            type: 'error',
            showCloseButton: true,
            actions: {
                accept: {
                    label: gettext('Подтвердить'),
                    action: function() {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data:{csrfmiddlewaretoken:csrftoken,ticket:ticketId},
                            dataType: 'json',
                            success: function (data) {
                                if(data == 1){
                                    $('#tickets').find('tr.table-ticket[data-ticket-id="'+ticketId+'"]').hide();
                                    return msg.update({
                                        message: gettext('Билет удалён'),
                                        type: 'success',
                                        actions: false,
                                        hideAfter: 2,
                                        hideOnNavigate: true
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    })

    /*----------------------------------------------------*/
    /*	Modal Game Payments Ticket
    /*----------------------------------------------------*/
    $(document).on('click','.payments-ticket',function(){
        var ticketId = $(this).parents('.table-ticket').data('ticket-id');
        $('#payments').data('ticket-id',ticketId);
        $('#payments').modal('show');
    })

    /*----------------------------------------------------*/
    /*	Choice of payment system
    /*----------------------------------------------------*/
    $('#payments a').click(function(){
        var payments = $(this).val(),
            typePay = $(this).data('payment-type'),
            url = '/cp/payment'+typePay+'/',
            ticket = $('#payments').data('ticket-id'),
            paymentSystem = $(this).attr('value'),
            csrftoken = getCookie('csrftoken');

        $.ajax({
            type: "POST",
            url: url,
            data:{csrfmiddlewaretoken:csrftoken,ticket:ticket,paymentSystem:paymentSystem},
            dataType: 'json',
            success: function (data) {
                if(typeof data == 'number'){
                    $('#tickets').find('tr.table-ticket[data-ticket-id="'+ticket+'"]').find('.ticket-status').html('<span class="label label-sm label-warning">'+gettext('Ожидание')+'</span>');
                    $('#tickets').find('tr.table-ticket[data-ticket-id="'+ticket+'"]').find('.payments-ticket').hide();
                    $('#payments').modal('hide');
                    if(typePay == "bonus"){
                        location.reload();
                    }
                    else{
                        location="/cp/invoice"+typePay+"/"+data+"/";
                    }
                }
            }
        });
        return false;
    })
})

