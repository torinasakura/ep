/**
 * Created by Andrew Ghostuhin.
 * Date: 11.12.13
 * Time: 17:10
 */

//Document ready
jQuery(document).ready(function($){

    /*----------------------------------------------------*/
    /*	Carousel Section
     /*----------------------------------------------------*/

    jQuery('.portfolio-carousel').carousel({interval: false, wrap: false});


    jQuery('.client-carousel').carousel({interval: false, wrap: false});

    jQuery('.testimonials-carousel').carousel({interval: 5000, pause: "hover"});



        $("a[rel^='prettyPhoto']").prettyPhoto({
            animation_speed: 'fast', /* fast/slow/normal */
            slideshow: 5000, /* false OR interval time in ms */
            autoplay_slideshow: false, /* true/false */
            opacity: 0.80, /* Value between 0 and 1 */
            show_title: true, /* true/false */
            allow_resize: true, /* Resize the photos bigger than viewport. true/false */
            default_width: 500,
            default_height: 344,
            counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
            theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
            horizontal_padding: 20, /* The padding on each side of the picture */
            hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
            wmode: 'opaque', /* Set the flash wmode attribute */
            autoplay: true, /* Automatically start videos: True/False */
            modal: false, /* If set to true, only the close button will close the window */
            deeplinking: true, /* Allow prettyPhoto to update the url to enable deeplinking. */
            overlay_gallery: true, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
            keyboard_shortcuts: true, /* Set to false if you open forms inside prettyPhoto */
            changepicturecallback: function(){}, /* Called everytime an item is shown/changed */
            callback: function(){}, /* Called when prettyPhoto is closed */
            ie6_fallback: true,
            markup: '<div class="pp_pic_holder"> \
						<div class="ppt">&nbsp;</div> \
						<div class="pp_top"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
						<div class="pp_content_container"> \
							<div class="pp_left"> \
							<div class="pp_right"> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade"> \
										<a href="#" class="pp_expand" title="Expand the image">Expand</a> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"></div> \
										<div class="pp_details"> \
											<div class="pp_nav"> \
												<a href="#" class="pp_arrow_previous">Previous</a> \
												<p class="currentTextHolder">0/0</p> \
												<a href="#" class="pp_arrow_next">Next</a> \
											</div> \
											<p class="pp_description"></p> \
											{pp_social} \
											<a class="pp_close" href="#">Close</a> \
										</div> \
									</div> \
								</div> \
							</div> \
							</div> \
						</div> \
						<div class="pp_bottom"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
					</div> \
					<div class="pp_overlay"></div>',
            gallery_markup: '<div class="pp_gallery"> \
								<a href="#" class="pp_arrow_previous">Previous</a> \
								<div> \
									<ul> \
										{gallery} \
									</ul> \
								</div> \
								<a href="#" class="pp_arrow_next">Next</a> \
							</div>',
            image_markup: '<img id="fullResImage" src="{path}" />',
            flash_markup: '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="{width}" height="{height}"><param name="wmode" value="{wmode}" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{path}" /><embed src="{path}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="{width}" height="{height}" wmode="{wmode}"></embed></object>',
            quicktime_markup: '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="{height}" width="{width}"><param name="src" value="{path}"><param name="autoplay" value="{autoplay}"><param name="type" value="video/quicktime"><embed src="{path}" height="{height}" width="{width}" autoplay="{autoplay}" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/"></embed></object>',
            iframe_markup: '<iframe src ="{path}" width="{width}" height="{height}" frameborder="no"></iframe>',
            inline_markup: '<div class="pp_inline">{content}</div>',
            custom_markup: '',
            social_tools: '<div class="pp_social"><div class="twitter"><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div><div class="facebook"><iframe src="http://www.facebook.com/plugins/like.php?locale=en_US&href='+location.href+'&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:23px;" allowTransparency="true"></iframe></div></div>' /* html or false to disable */
        });

    /*----------------------------------------------------*/
    /*	Hover Overlay
     /*----------------------------------------------------*/

        jQuery('.portfolio-item').hover(function () {
            jQuery(this).find( '.portfolio-item-hover' ).animate({
                "opacity": 0.8
            }, 100, 'easeInOutCubic');


        }, function () {
            jQuery(this).find( '.portfolio-item-hover' ).animate({
                "opacity": 0
            }, 100, 'easeInOutCubic');

        });


        jQuery('.portfolio-item').hover(function () {
            jQuery(this).find(".fullscreen").stop().animate({'top' : '60%', 'opacity' : 1}, 250, 'easeOutBack');

        }, function () {
            jQuery(this).find(".fullscreen").stop().animate({'top' : '65%', 'opacity' : 0}, 150, 'easeOutBack');

        });


        jQuery('.blog-showcase ul li').each(function () {
            jQuery(this).on('hover', function () {
                jQuery(this).siblings('li').removeClass('blog-first-el').end().addClass('blog-first-el');
            });
        });


        jQuery('.blog-showcase-thumb').hover(function () {
            jQuery(this).find( '.post-item-hover' ).animate({
                "opacity": 0.8
            }, 100, 'easeInOutCubic');

        }, function () {
            jQuery(this).find( '.post-item-hover' ).animate({
                "opacity": 0
            }, 100, 'easeInOutCubic');

        });



        jQuery('.blog-showcase-thumb').hover(function () {
            jQuery(this).find(".fullscreen").stop().animate({'top' : '57%', 'opacity' : 1}, 250, 'easeOutBack');

        }, function () {
            jQuery(this).find(".fullscreen").stop().animate({'top' : '65%', 'opacity' : 0}, 150, 'easeOutBack');

        });



        /* Post Image overlay */

        jQuery('.post-image').hover(function () {
            jQuery(this).find( '.img-hover' ).animate({
                "opacity": 0.8
            }, 100, 'easeInOutCubic');


        }, function () {
            jQuery(this).find( '.img-hover' ).animate({
                "opacity": 0
            }, 100, 'easeInOutCubic');

        });


        jQuery('.post-image').hover(function () {
            jQuery(this).find(".fullscreen").stop().animate({'top' : '55%', 'opacity' : 1}, 250, 'easeOutBack');

        }, function () {
            jQuery(this).find(".fullscreen").stop().animate({'top' : '65%', 'opacity' : 0}, 150, 'easeOutBack');

        });


        /*Mobile device topnav opener*/

        jQuery( ".down-button" ).click(function() {
            jQuery( ".down-button .icon-current" ).toggleClass("icon-angle-up icon-angle-down");
        });

    //MASONRY
    var $container = $('#content');
    $container.imagesLoaded( function(){
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
    });
    $('#filter a').click(function (event) {
        $('a.selected').removeClass('selected');
        var $this = $(this);
        $this.addClass('selected');
        var selector = $this.attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });

    //ROLL ON HOVER
    $(".roll").css("opacity","0");
    $(".roll").hover(function () {
            $(this).stop().animate({
                opacity: .8
            }, "slow");
        },
        function () {
            $(this).stop().animate({
                opacity: 0
            }, "slow");
        });

    /*----------------------------------------------------*/
    /*	Data Table
    /*----------------------------------------------------*/
    $('#archives').dataTable();

    $('table th input:checkbox').on('click' , function(){
        var that = this;
        $(this).closest('table').find('tr > td:first-child input:checkbox')
            .each(function(){
                this.checked = that.checked;
                $(this).closest('tr').toggleClass('selected');
            });

    });

    $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
    function tooltip_placement(context, source) {
        var $source = $(source);
        var $parent = $source.closest('table')
        var off1 = $parent.offset();
        var w1 = $parent.width();

        var off2 = $source.offset();
        var w2 = $source.width();

        if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
        return 'left';
    }

    //Select
    $(".select2").css('width','200px').select2({allowClear:true})
        .on('change', function(){
            $(this).closest('form').validate().element($(this));
        });

    // Autocomplete
    var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme"
    ];
    $( "#tags" ).autocomplete({
        source: availableTags
    });

    //Категории для autocomplete
    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function( ul, items ) {
            var that = this,
                currentCategory = "";
            $.each( items, function( index, item ) {
                if ( item.category != currentCategory ) {
                    ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                    currentCategory = item.category;
                }
                that._renderItemData( ul, item );
            });
        }
    });

    var data = [
        { label: "Feifer", category: "" },
        { label: "Ghost", category: "" },
        { label: "Axel", category: "" },
        { label: "Python", category: "Products" },
        { label: "Django", category: "Products" },
        { label: "Bootstrap", category: "Products" },
        { label: "Andrew", category: "People" },
        { label: "Sergej", category: "People" },
        { label: "Maximiliano", category: "People" }
    ];
    $( "#city" ).catcomplete({
        delay: 0,
        source: data
    });

    // Wizard
    $('[data-rel=tooltip]').tooltip();

    $(".country").css('width','200px').select2({allowClear:true})
        .on('change', function(){
            $(this).closest('form').validate().element($(this));
        });


    var $validation = false;

    $('#switcher').removeAttr('checked').on('click', function(){
        $validation = this.checked;
        if(this.checked) {
            $('#regform').hide();
            $('#loginform').removeClass('hide');
        }
        else {
            $('#loginform').addClass('hide');
            $('#regform').show();
        }
    });

    /*----------------------------------------------------*/
    /*	Filled form
    /*----------------------------------------------------*/

    var dayBox = $('#reg_form').find('select[name = day]'),
        monthBox = $('#reg_form').find('select[name = month]'),
        yearBox = $('#reg_form').find('select[name = year]'),
        date = new Date(),
        yearSelect = '';

    //Days
    function creationDays(num){
        var daySelect = '<option selected disabled>'+ gettext('День') +':</option>';

        for(i = 1,day = 1; i <=num; i++, day++){
            if(i < 10){
                day = '0' + day;
            }
            daySelect += '<option value="'+day+'">'+day+'</option>';
        }
        dayBox.val('');
        dayBox.html(daySelect);
    }
    creationDays(31);

    //Years
    var maxYear = date.getFullYear() - 18;
    for(i = maxYear; i >= 1914; i--){
        yearSelect += '<option value="'+i+'">'+i+'</option>';
    }
    yearBox.append(yearSelect);

    /*----------------------------------------------------*/
    /*	Chang Month
    /*----------------------------------------------------*/
    var month = {
        '01': '31',
        '02': '29',
        '03': '31',
        '04': '30',
        '05': '31',
        '06': '30',
        '07': '31',
        '08': '31',
        '09': '30',
        '10': '31',
        '11': '30',
        '12': '31'
    }

    monthBox.change(function(){
        var currentMonth = $(this).val(),
            countDays = month[currentMonth];

        creationDays(countDays);
    })

    $('#reg_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            login: {
                required: true,
                minlength: 6,
                maxlength: 16
            },
            name: {
                required: true,
                minlength: 2,
                maxlength: 20
            },
            surname: {
                required: true,
                minlength: 2,
                maxlength: 20
            },
            day: {
                required: true
            },
            month: {
                required: true
            },
            year: {
                required: true
            },
            city: {
                required: true
            },
            country: {
                required: true
            },
            phone: {
                required: true,
                minlength: 6
            },
            skype: {
                required: false,
                minlength: 6,
                maxlength: 32
            },
            email: {
                required: true,
                email:true
            },
            password: {
                required: true,
                minlength: 5
            },
            password2: {
                required: true,
                minlength: 5,
                equalTo: '#password'
            },
            agree: 'required'
        },

        messages: {
            login: {
                required: gettext('Пожалуйста, придумайте логин, иначе вы не сможете войти в систему.'),
                minlength: gettext('Минимальная длина логина должна быть не меньше 6 символов, без спецзнаков и пробелов.'),
                maxlength: gettext('Максимальная длина логина должна быть не более 16 символов, без спецзнаков и пробелов.')
            },
            name: {
                required: gettext('Пожалуйста, введите ваше имя.'),
                minlength: gettext('Минимальная длина имени должна быть не меньше 2 символов, без спецзнаков и пробелов.'),
                maxlength: gettext('Максимальная длина имени должна быть не более 20 символов, без спецзнаков и пробелов.')
            },
            surname: {
                required: gettext('Пожалуйста, введите вашу фамилию.'),
                minlength: gettext('Минимальная длина фамилии должна быть не меньше 2 символов, без спецзнаков и пробелов.'),
                maxlength: gettext('Максимальная длина фамилии должна быть не более 20 символов, без спецзнаков и пробелов.')
            },
            day: gettext('Пожалуйста, укажите день вашего рождения'),
            month: gettext('Пожалуйста, укажите месяц вашего рождения'),
            year: gettext('Пожалуйста, укажите год вашего рождения'),
            city: gettext('Пожалуйста, укажите в каком городе вы проживаете.'),
            country: gettext('Пожалуйста, укажите в какой стране вы проживаете'),
            phone:{
                required: gettext('Пожалуйста, введите ваш номер телефона.'),
                minlength: gettext('Пожалуйста, введите корретный номер телефона.')
            },
            skype: {
                minlength: gettext('Минимальная длина логина skype 6 символов, без спецзнаков и пробелов.'),
                maxlength: gettext('Максимальная длина логина skype 32 символа, без спецзнаков и пробелов.')
            },
            email: {
                required: gettext('Пожалуйста, введите корретный email.'),
                email: gettext('Пожалуйста, укажите ваш email.')
            },
            password: {
                required: gettext('Пожалуйста, введите пароль.'),
                minlength: gettext('Пожалуйста, создайте более надёжный пароль.')
            },
            password2: {
                required: gettext('Пожалуйста, повторите пароль.'),
                minlength: gettext('Пожалуйста, создайте более надёжный пароль.'),
                equalTo: gettext('Пароли должны совпадать.')
            },
            agree: gettext('Для окончания регистрации вы должны согласится с условиями.')
        },

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success').addClass('has-error');//Заменил has-info на has-success
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-success');//Заменил has-info на has-success
            $(e).remove();
        },
        submitHandler: function(form) {
            var b_day = $(form).find('select[name = day]').val(),
                b_month = $(form).find('select[name = month]').val(),
                b_year = $(form).find('select[name = year]').val(),
                birthday = '';

            if(b_day && b_month && b_year){
                birthday = b_day + '/' + b_month + '/' + b_year;
                $(form).find('input[name = birthday]').val(birthday);
                form.submit();
            }
        }
    });
    $('#login_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            login: {
                required: true,
                minlength: 6,
                maxlength: 16
            },
            password: {
                required: true,
                minlength: 5
            }
        },

        messages: {
            login: {
                required: gettext('Пожалуйста, введите логин, иначе вы не сможете войти в систему.'),
                minlength: gettext('Минимальная длина логина должна быть не меньше 6 символов, без спецзнаков и пробелов.'),
                maxlength: gettext('Максимальная длина логина должна быть не более 16 символов, без спецзнаков и пробелов.')
            },
            password: {
                required: gettext('Пожалуйста, введите пароль.'),
                minlength: gettext('Минимальная длина пароля должна быть не меньше 5 символов.')
            }
        },

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success').addClass('has-error');//Заменил has-info на has-success
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-success');//Заменил has-info на has-success
            $(e).remove();
        }
    });

    /*----------------------------------------------------*/
    /*	Select Language
    /*----------------------------------------------------*/

    $('#language_form').find('.select-language').click(function(){
        var lng = $(this).data('value');
        $('#language_form').find('input[name="language"]').val(lng);
        $('#language_form').submit();
    })

});

