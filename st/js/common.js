//----MENU---//
$('.navbar .dropdown').hover(function() {
	$(this).addClass('extra-nav-class').find('.dropdown-menu').first().stop(true, true).delay(50).slideDown();
}, function() {
	var na = $(this)
	na.find('.dropdown-menu').first().stop(true, true).delay(100).slideUp('fast', function(){ na.removeClass('extra-nav-class') })
});

//MENU RESPONSIVE
 $(document).ready(function(){
$("#nav").tinyNav({
  active: 'selected', // String: Set the "active" class
  header: 'MENU +', // String: Specify text for "header" and show header instead of the active item
  label: '' // String: Sets the <label> text for the <select> (if not set, no label will be added)
});
});

//----HEADER---//
jQuery(window).scroll(function () {
  if (jQuery(document).scrollTop() == 0) {
    jQuery('.wowmenu').removeClass('tiny');
  } else {
    jQuery('.wowmenu').addClass('tiny');
  }
});

//----NICE SCROLL---//  
jQuery(document).ready(function($){
    $("html").niceScroll({
			scrollspeed: 60,
			mousescrollstep: 40,
			cursorwidth: 10,
			cursorheight: 130,
			cursorborder: 0,
			cursorcolor: '#636363',
			cursorborderradius: 5,
			styler:"fb",
			autohidemode: false,
			horizrailenabled: false
		});
  }
);


//----FOOTER TESTIMONIAL---//  
jQuery(document).ready(function ($) {
$('.textItem').quovolver();
  });

//----TO TOP---//
jQuery(document).ready(function($){
	// hide #back-top first
	$("#back-top").hide();	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 600) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
});



	//YUMMI LOADER
	var $body = $('body');
	$(window).load(function() {
		$body.toggleClass('on off');
		$('#trigger').click(function() {
			$body.toggleClass('on off');
			setTimeout(function() {
				$body.toggleClass('on off');
			}, 2000)
		});
	}); 
	
	  
//////----Placeholder for IE---////////
$(function() {
    // Invoke the plugin
    $('input, textarea').placeholder();
  });

//----ANIMATIONS---//
jQuery(document).ready(function($){

jQuery('.animated').appear();

    jQuery(document.body).on('appear', '.fade', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('anim-fade') });
    });
    jQuery(document.body).on('appear', '.slidea', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('anim-slide') });
    });
    jQuery(document.body).on('appear', '.hatch', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('anim-hatch') });
    });
    jQuery(document.body).on('appear', '.entrance', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('anim-entrance') });
    });
	jQuery(document.body).on('appear', '.fadeInUpNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('fadeInUp') });
    });
	jQuery(document.body).on('appear', '.fadeInDownNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('fadeInDown') });
    });
	jQuery(document.body).on('appear', '.fadeInLeftNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('fadeInLeft') });
    });
	jQuery(document.body).on('appear', '.fadeInRightNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('fadeInRight') });
    });
	
	
	jQuery(document.body).on('appear', '.fadeInUpBigNow', function() {
    jQuery(this).each(function(){ jQuery(this).addClass('fadeInUpBig') });
    });
	jQuery(document.body).on('appear', '.fadeInDownBigNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('fadeInDownBig') });
    });
	jQuery(document.body).on('appear', '.fadeInLeftBigNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('fadeInLeftBig') });
    });
	jQuery(document.body).on('appear', '.fadeInRightBigNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('fadeInRightBig') });
    });
	
	jQuery(document.body).on('appear', '.fadeInNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('fadeIn') });
    });
	jQuery(document.body).on('appear', '.flashNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('flash') });
    });
	jQuery(document.body).on('appear', '.shakeNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('shake') });
    });
	jQuery(document.body).on('appear', '.bounceNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('bounce') });
    });
	jQuery(document.body).on('appear', '.tadaNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('tada') });
    });
	jQuery(document.body).on('appear', '.swingNow', function() {
        jQuery(this).each(function(){ jQuery(this).addClass('swing') });
    });
});

//----------Contact Form------------//
jQuery(document).ready(function($){

    //---Contact Form Validation---//
    function formValidation(form){
        var errorMessage = false;

        form.find('input, textarea').each(function(){
            if($(this).val() != ''){
                $(this).removeClass('error');
            } else {
                $(this).addClass('error');
                errorMessage = "Не все поля заполнены";
            }
        })

        var sizeEmpty = form.find('.error').length;

        if(sizeEmpty == 0){
            //Mail
            var mail = form.find('input[name="email"]').val();
            if(mail){
                var pattern = /^([a-z0-9_\-\.])+\@([a-z0-9_\-\.])+\.([a-z]{2,4})$/i
                if(pattern.test(mail)){
                    form.find('input[name="email"]').removeClass('error');
                }
                else{
                    form.find('input[name="email"]').addClass('error');
                    errorMessage = "Неверный формат E-Mail";
                }
            }
        }
        return errorMessage;
    }

    //-- Footer Contact Form --//
    $('#contactform').submit(function(e){
        e.preventDefault();

        var form = $(this),
            error = formValidation(form),
            url = $(this).attr('action'),
            name = form.find('input[name=name]').val(),
            email = form.find('input[name=email]').val(),
            message = form.find('textarea[name=message]').val(),
            csrfmiddlewaretoken = form.find('input[name=csrfmiddlewaretoken]').val();

        if(error){
            $('.done').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>'+error+'</div>').fadeIn('slow');
            return false;
        }

        form.find('input.error,textarea.error').removeClass('error');

        $.ajax({
            url: url,
            type: "POST",
            data: {name:name,email:email,message:message,csrfmiddlewaretoken:csrfmiddlewaretoken},
            cache: false,
            success: function (html) {
                if (html==1) {
                    jQuery('.done').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Ваше сообщение доставленно. Спасибо!</div>').fadeIn('slow');
                    form.find('input[type=text], textarea').val("");

                } else{
                    jQuery('.done').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>В настоящий момент сервис недоступен. Пожалуйста, повторите попытку позже.</div>').fadeIn('slow');
                }
            }
        })
        return false;
    })
})


