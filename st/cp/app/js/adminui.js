/**
 * User: Andrew Ghostuhin
 * Redaction: Max Zotkin
 * Date: 10.01.14
 * Time: 12:37
 */

jQuery(document).ready(function($){

    /*----------------------------------------------------*/
    /*	Bonus Tree
    /*----------------------------------------------------*/
    jQuery('.tree-toggle').click(function () {
        var status = jQuery(this).hasClass('tree-open');
        if(status){
            jQuery(this).removeClass('tree-open ion-person').addClass('tree-close ion-person-add');
            $(this).parent().children('ul.tree').hide(200);
        }
        else{
            jQuery(this).removeClass('tree-close ion-person-add').addClass('tree-open ion-person');
            $(this).parent().children('ul.tree').show(200);
        }
    });

    /*----------------------------------------------------*/
    /*	Validation form
    /*----------------------------------------------------*/

    $('#addbank_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            bankname: {
                required: true
            },
            bankaddress: {
                required: true
            },
            recipientname: {
                required: true
            },
            recipientaddress: {
                required: true
            },
            number: {
                required: true
            },
            bic: {
                required: true
            },
            swift: {
                required: true
            },
            logo: {
                required: true
            }
        },

        messages: {
            bankname: gettext("Пожалуйста, укажите наименование банка"),
            bankaddress: gettext("Пожалуйста, укажите адрес банка."),
            recipientname: gettext("Пожалуйста, укажите имя получателя"),
            recipientaddress: gettext("Пожалуйста, укажите адрес получателя"),
            number: gettext("Пожалуйста, укажите номер счета."),
            bic: gettext("Пожалуйста, укажите BIC"),
            swift: gettext("Пожалуйста, укажите SWIFT"),
            logo: gettext("Пожалуйста, добавьте логотип банка.")
        },

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success').addClass('has-error');//Заменил has-info на has-success
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-success');//Заменил has-info на has-success
            $(e).remove();
        }
    });

    $('#addpurse_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            name: {
                required: true
            },
            number: {
                required: true
            },
            logo: {
                required: true
            }
        },

        messages: {
            name: gettext("Пожалуйста, укажите название платежной системы."),
            number: gettext("Пожалуйста, укажите номер кошелька."),
            logo: gettext("Пожалуйста, добавьте логотип платежной системы.")
        },

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success').addClass('has-error');//Заменил has-info на has-success
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-success');//Заменил has-info на has-success
            $(e).remove();
        }
    });

    /*----------------------------------------------------*/
    /*	Edit Ticket form
    /*----------------------------------------------------*/
    $('#editticket_form').submit(function(){
        var inputDate = $('#editticket_form').find('input[name="date"]'),
            date = inputDate.val(),
            time = inputDate.attr('placeholder');

        inputDate.val(date+' '+time);
    })

    /*----------------------------------------------------*/
    /*	getCookie
    /*----------------------------------------------------*/
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    /*----------------------------------------------------*/
    /*	On Off Remove Bank
    /*----------------------------------------------------*/
    $('#banks_table').on('click','.remove-bank',function(){
        var bankId = $(this).parents('.table-bank').data('bank-id'),
            csrftoken = getCookie('csrftoken'),
            url = '/adminui/rembank/'+bankId+'/';

        var msg = Messenger().post({
            message: gettext('Вы уверены, что хотите удалить банк?'),
            type: 'error',
            showCloseButton: true,
            actions: {
                accept: {
                    label: gettext('Подтвердить'),
                    action: function() {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data:{csrfmiddlewaretoken:csrftoken},
                            dataType: 'json',
                            success: function (data) {
                                if(data == 1){
                                    $('#banks_table').find('tr.table-bank[data-bank-id="'+bankId+'"]').hide();
                                    return msg.update({
                                        message: gettext('Банк удалён'),
                                        type: 'success',
                                        actions: false,
                                        hideAfter: 2,
                                        hideOnNavigate: true
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    })

    $('#banks_table').on('click','.on-bank',function(){
        var bankId = $(this).parents('.table-bank').data('bank-id'),
            csrftoken = getCookie('csrftoken'),
            url = '/adminui/onbank/'+bankId+'/';

        $.ajax({
            type: "POST",
            url: url,
            data:{csrfmiddlewaretoken:csrftoken},
            dataType: 'json',
            success: function (data) {
                if(data == 1){
                    var bank = $('#banks_table').find('tr.table-bank[data-bank-id="'+bankId+'"]');
                    bank.find('.bank-status').html('<span class="label label-sm label-success">'+gettext('Активен')+'</span>');
                    bank.find('.bank-button').html('<a href="#"  role="button" class="btn btn-sm btn-ion btn-danger off-bank">'+gettext('Отключить')+'<i class="icon ion-ios7-trash"></i></a>'+
                                                   '<a href="#"  role="button" class="btn btn-sm btn-ion btn-danger remove-bank">'+gettext('Удалить')+'<i class="icon ion-ios7-trash"></i></a>');
                    Messenger().post({
                        message: gettext('Банк включен'),
                        type: 'success',
                        actions: false,
                        hideAfter: 2,
                        hideOnNavigate: true
                    });
                }
            }
        });
    })
    $('#banks_table').on('click','.off-bank',function(){
        var bankId = $(this).parents('.table-bank').data('bank-id'),
            csrftoken = getCookie('csrftoken'),
            url = '/adminui/offbank/'+bankId+'/';

        $.ajax({
            type: "POST",
            url: url,
            data:{csrfmiddlewaretoken:csrftoken},
            dataType: 'json',
            success: function (data) {
                if(data == 1){
                    var bank = $('#banks_table').find('tr.table-bank[data-bank-id="'+bankId+'"]');
                    bank.find('.bank-status').html('<span class="label label-sm label-danger">'+gettext('Не активен')+'</span>');
                    bank.find('.bank-button').html('<a href="#" role="button" class="btn btn-sm btn-ion btn-success on-bank">'+gettext('Включить')+'<i class="icon ion-card"></i></a>'+
                        '<a href="#"  role="button" class="btn btn-sm btn-ion btn-danger remove-bank">'+gettext('Удалить')+'<i class="icon ion-ios7-trash"></i></a>');
                    Messenger().post({
                        message: gettext('Банк отключен'),
                        type: 'success',
                        actions: false,
                        hideAfter: 2,
                        hideOnNavigate: true
                    });
                }
            }
        });
    })

    /*----------------------------------------------------*/
    /*	On Off Remove Purses
    /*----------------------------------------------------*/
    $('#purses_table').on('click','.remove-purse',function(){
        var purseId = $(this).parents('.table-purse').data('purse-id'),
            csrftoken = getCookie('csrftoken'),
            url = '/adminui/rempurse/'+purseId+'/';

        var msg = Messenger().post({
            message: gettext('Вы уверены, что хотите удалить платежную систему?'),
            type: 'error',
            showCloseButton: true,
            actions: {
                accept: {
                    label: gettext('Подтвердить'),
                    action: function() {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data:{csrfmiddlewaretoken:csrftoken},
                            dataType: 'json',
                            success: function (data) {
                                if(data == 1){
                                    $('#purses_table').find('tr.table-purse[data-purse-id="'+purseId+'"]').hide();
                                    return msg.update({
                                        message: gettext('Платежная система удалена'),
                                        type: 'success',
                                        actions: false,
                                        hideAfter: 2,
                                        hideOnNavigate: true
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    })
    $('#purses_table').on('click','.on-purse',function(){
        var purseId = $(this).parents('.table-purse').data('purse-id'),
            csrftoken = getCookie('csrftoken'),
            url = '/adminui/onpurse/'+purseId+'/';

        $.ajax({
            type: "POST",
            url: url,
            data:{csrfmiddlewaretoken:csrftoken},
            dataType: 'json',
            success: function (data) {
                if(data == 1){
                    var purse = $('#purses_table').find('tr.table-purse[data-purse-id="'+purseId+'"]');
                    purse.find('.purse-status').html('<span class="label label-sm label-success">'+gettext('Активен')+'</span>');
                    purse.find('.purse-button').html('<a href="#"  role="button" class="btn btn-sm btn-ion btn-danger off-purse">'+gettext('Отключить')+'<i class="icon ion-ios7-trash"></i></a>'+
                        '<a href="#"  role="button" class="btn btn-sm btn-ion btn-danger remove-purse">'+gettext('Удалить')+'<i class="icon ion-ios7-trash"></i></a>');
                    Messenger().post({
                        message: gettext('Платежная система включена'),
                        type: 'success',
                        actions: false,
                        hideAfter: 2,
                        hideOnNavigate: true
                    });
                }
            }
        });
    })
    $('#purses_table').on('click','.off-purse',function(){
        var purseId = $(this).parents('.table-purse').data('purse-id'),
            csrftoken = getCookie('csrftoken'),
            url = '/adminui/offpurse/'+purseId+'/';

        $.ajax({
            type: "POST",
            url: url,
            data:{csrfmiddlewaretoken:csrftoken},
            dataType: 'json',
            success: function (data) {
                if(data == 1){
                    var purse = $('#purses_table').find('tr.table-purse[data-purse-id="'+purseId+'"]');
                    purse.find('.purse-status').html('<span class="label label-sm label-danger">'+gettext('Не активен')+'</span>');
                    purse.find('.purse-button').html('<a href="#" role="button" class="btn btn-sm btn-ion btn-success on-purse">'+gettext('Включить')+'<i class="icon ion-card"></i></a>'+
                        '<a href="#"  role="button" class="btn btn-sm btn-ion btn-danger remove-purse">'+gettext('Удалить')+'<i class="icon ion-ios7-trash"></i></a>');
                    Messenger().post({
                        message: gettext('Платежная система отключена'),
                        type: 'success',
                        actions: false,
                        hideAfter: 2,
                        hideOnNavigate: true
                    });
                }
            }
        });
    })

    /*----------------------------------------------------*/
    /*	Remove Ticket
    /*----------------------------------------------------*/
    $('#tickets').on('click','.remove-ticket',function(){
        var ticketId = $(this).parents('.table-ticket').data('ticket-id'),
            csrftoken = getCookie('csrftoken'),
            url = '/adminui/remticket/'+ticketId+'/';

        var msg = Messenger().post({
            message: gettext('Вы уверены, что хотите удалить билет?'),
            type: 'error',
            showCloseButton: true,
            actions: {
                accept: {
                    label: gettext('Подтвердить'),
                    action: function() {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data:{csrfmiddlewaretoken:csrftoken},
                            dataType: 'json',
                            success: function (data) {
                                if(data == 1){
                                    $('#tickets').find('tr.table-ticket[data-ticket-id="'+ticketId+'"]').hide();
                                    return msg.update({
                                        message: gettext('Билет удалён'),
                                        type: 'success',
                                        actions: false,
                                        hideAfter: 2,
                                        hideOnNavigate: true
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    })
})
