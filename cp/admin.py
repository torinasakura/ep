from django.contrib import admin
from models import Userinfo, Partners, Invoicep, Invoiceb, Message, Bonus, Profit, Wreq, Invoicebonus


admin.site.register(Userinfo)
admin.site.register(Partners)
admin.site.register(Bonus)
admin.site.register(Invoicep)
admin.site.register(Invoiceb)
admin.site.register(Invoicebonus)
admin.site.register(Message)
admin.site.register(Profit)
admin.site.register(Wreq)


