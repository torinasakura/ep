from django.db import models
from django.contrib.auth.models import User
from index.models import Game, Country, State, Ticket
from adminui.models import Bank, Purse


class Userinfo(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Пользователь')
    country = models.ForeignKey(Country, verbose_name=u'Страна')
    city = models.ForeignKey(State, verbose_name=u'Город')
    phone = models.CharField(max_length=32, verbose_name=u'Телефон')
    skype = models.CharField(max_length=32, blank=True, verbose_name=u'Скайп')
    bday = models.DateField(verbose_name=u'Дата рождения')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u"Дополнительная информация"
        verbose_name_plural = u"Дополнительная информация"


class Partners(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Пользователь')
    bal = models.DecimalField(max_digits=12, decimal_places=3, default=0, verbose_name=u'Баланс')
    countp = models.IntegerField(default=0, verbose_name=u'Патрнеров')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u"Бонусная программа"
        verbose_name_plural = u"Бонусные программы"


class Bonus(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    refer = models.ForeignKey(User, verbose_name=u'Реферал', related_name=u'bonus_refer')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u"Партнер"
        verbose_name_plural = u"Партнеры"


class Profit(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    refer = models.ForeignKey(User, verbose_name=u'Реферал', related_name=u'profit_refer')
    profit = models.DecimalField(max_digits=12, decimal_places=3, default=0, verbose_name=u'Прибыль')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u"Прибыль"
        verbose_name_plural = u"Прибыль"


class Emailv(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Пользователь')
    key = models.CharField(max_length=32, verbose_name=u'Ключ')

    def __unicode__(self):
        return self.key

    class Meta:
        verbose_name = u"Ключ"
        verbose_name_plural = u"Ключи"


class Rpass(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Пользователь')
    key = models.CharField(max_length=32, verbose_name=u'Ключ')

    def __unicode__(self):
        return self.key

    class Meta:
        verbose_name = u"Rpass"
        verbose_name_plural = u"Rpass"


class Invoiceb(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    status = models.IntegerField(default=0, verbose_name=u'Статус')
    date = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата начала')
    type = models.IntegerField(default=0, verbose_name=u'Тип')
    system = models.ForeignKey(Bank, verbose_name=u'Платежная система')
    ticket = models.OneToOneField(Ticket, verbose_name=u'Билет')
    price = models.IntegerField(default=0, verbose_name=u'Цена')

    def __unicode__(self):
        return self.system.bankname

    class Meta:
        verbose_name = u"Invoice bank"
        verbose_name_plural = u"Invoice bank"


class Invoicep(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    status = models.IntegerField(default=0, verbose_name=u'Статус')
    date = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата начала')
    type = models.IntegerField(default=0, verbose_name=u'Тип')
    system = models.ForeignKey(Purse, verbose_name=u'Платежная система')
    ticket = models.OneToOneField(Ticket, verbose_name=u'Билет')
    price = models.IntegerField(default=0, verbose_name=u'Цена')

    def __unicode__(self):
        return self.system.name

    class Meta:
        verbose_name = u"Invoice purse"
        verbose_name_plural = u"Invoice purse"


class Invoicebonus(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    date = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата начала')
    ticket = models.OneToOneField(Ticket, verbose_name=u'Билет')
    price = models.IntegerField(default=0, verbose_name=u'Цена')

    def __unicode__(self):
        return str(self.ticket.number)

    class Meta:
        verbose_name = u"Invoice bonus"
        verbose_name_plural = u"Invoice bonus"


class Message(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    date = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата начала')
    read = models.BooleanField(verbose_name=u'Активно')
    title = models.CharField(max_length=500, verbose_name=u'Заголовок')
    text = models.TextField(verbose_name=u'Текст сообщения')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u"Перманентный алерт"
        verbose_name_plural = u"Перманентные алерты"


class Wreq(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    status = models.IntegerField(default=0, verbose_name=u'Статус')
    points = models.DecimalField(max_digits=12, decimal_places=3, default=0, verbose_name=u'Количество средств')
    name = models.CharField(max_length=500, verbose_name=u'Имя получателя')
    address = models.CharField(max_length=500, verbose_name=u'Адрес получателя')
    bankname = models.CharField(max_length=500, verbose_name=u'Наименование Банка')
    bankaddress = models.CharField(max_length=500, verbose_name=u'Адрес Банка')
    number = models.CharField(max_length=500, verbose_name=u'Номер счета')
    iban = models.CharField(max_length=500, verbose_name=u'IBAN')
    swift = models.CharField(max_length=500, verbose_name=u'SWIFT')

    def __unicode__(self):
        return self.bankname

    class Meta:
        verbose_name = u"Вывод средств"
        verbose_name_plural = u"Выводы средств"