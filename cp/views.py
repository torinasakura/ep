from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from index.models import Game, Ticket, Series, Country, State
from models import Invoicep, Invoiceb, Message, Userinfo, Wreq, Profit, Bonus, Invoicebonus
from adminui.models import Bank, Purse
from django.http import HttpResponse
from django.core.urlresolvers import reverse
import json
from decimal import Decimal
from datetime import datetime
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


@login_required(login_url='/reg/')
def profile(request):
    if request.method == "POST":
        try:
            u = request.user
            ui = u.userinfo
            bday = datetime.strptime(request.POST['birthday'], '%d/%m/%Y')
            email = request.POST['email']
            country = request.POST['country']
            city = request.POST['city']
            phone = request.POST['phone']
            try:
                skype = request.POST['skype']
                ui.skype = skype
            except:
                print 'user do not use skype'
            u.email = email
            c = Country.objects.get(pcode=country)
            ui.country = c
            s = State.objects.get(code=city, count=c)
            ui.city = s
            ui.phone = phone
            ui.bday = bday
            ui.save()
            u.save()
            request.session['alert'] = _(u'Данные успешно обновленны')
            return HttpResponseRedirect('/cp/')
        except:
            request.session['alert'] = _(u'Возникла непредвиденная ошибка.')
            return render(request, 'cp/profile.html', {})
    return render(request, 'cp/profile.html', {})


@login_required(login_url='/reg/')
def bonus(request):
    p = Profit.objects.filter(refer=request.user)
    context = {'title': '| Бонусная программа',
               'p': p}
    return render(request, 'cp/bonus.html', context)


@login_required(login_url='/reg/')
def payments(request):
    ip = Invoicep.objects.filter(user=request.user)
    ib = Invoiceb.objects.filter(user=request.user)
    ibonus = Invoicebonus.objects.filter(user=request.user)
    p = Bonus.objects.filter(refer=request.user)
    users = []
    for item in p:
        users.append(item.user)
    referals = []
    for item in p:
        prof = Profit.objects.get(refer=request.user, user=item.user)
        referals.append({'user': item.user, 'profit': prof})
    sp = Profit.objects.filter(refer=request.user).exclude(user__in=users)
    context = {'title': _(u'| Платежи'),
               'invoicep': ip,
               'invoiceb': ib,
               'invoicebonus': ibonus,
               '1l': referals,
               'sp': sp,
    }
    return render(request, 'cp/payments.html', context)


@login_required(login_url='/reg/')
def tickets(request):
    b = Bank.objects.all()
    p = Purse.objects.all()
    t = Ticket.objects.all().filter(user=request.user)
    context = {'title': _(u'| Мои билеты'),
               'tickets': t,
               'banks': b,
               'purses': p}
    return render(request, 'cp/tickets.html', context)


@login_required(login_url='/reg/')
def printinvp(request, id):
    i = get_object_or_404(Invoicep, id=id)
    if i.user == request.user:
        context = {'i': i}
        return render(request, 'cp/printinvp.html', context)
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def printinvb(request, id):
    i = get_object_or_404(Invoiceb, id=id)
    if i.user == request.user:
        context = {'i': i}
        return render(request, 'cp/printinvb.html', context)
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def printinvbonus(request, id):
    i = get_object_or_404(Invoicebonus, id=id)
    if i.user == request.user:
        context = {'i': i}
        return render(request, 'cp/printinvbonus.html', context)
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def notification(request):
    m = Message.objects.all().filter(user=request.user).order_by('-date')
    context = {'title': _(u'| Уведомления системы'),
               'message': m}
    return render(request, 'cp/notification.html', context)


@login_required(login_url='/reg/')
def paymentbonus(request):
    if request.method == "POST":
        try:
            ticket = request.POST['ticket']
            t = Ticket.objects.get(id=ticket)
            ui = request.user.partners
            if ui.bal < t.price:
                request.session['alert'] = _(u'Недостаточно бонусов на счету.')
                return HttpResponseRedirect('/cp/tickets/')
            ui.bal = ui.bal - t.price
            ui.save()
            t.status = 4
            t.save()
            i = Invoicebonus()
            i.user = request.user
            i.ticket = t
            i.save()
            m = Message()
            m.user = request.user
            m.read = False
            m.title = _(u'Вы успешно оплатили бонусами билет №%s') % t.id
            m.text = _(u'Вы успешно оплатили бонусами билет №%s') % t.id
            m.save()
            return HttpResponse(1)
        except:
            return HttpResponse('err')


@login_required(login_url='/reg/')
def paymentp(request):
    if request.method == "POST":
        try:
            ps = request.POST['paymentSystem']
            ticket = request.POST['ticket']
            t = Ticket.objects.get(id=ticket)
            i = Invoicep()
            i.user = request.user
            i.status = 0
            i.type = 2
            p = Purse.objects.get(id=ps)
            i.system = p
            i.ticket = t
            t.status = 1
            t.save()
            i.price = t.price
            i.save()
            m = Message()
            m.user = request.user
            m.read = False
            m.title = _(u'Заявка на оплату принята')
            m.text = _(u'Ваша заявка была принята на рассмотрение ознакомится с ней вы можете по ссылке'
                       u' <a href="http://euro-paradise.com/cp/invoicep/{0}/">Счет номер {1}</a>'.format(i.id, i.id))
            m.save()
            return HttpResponse(i.id)
        except:
            return HttpResponse('err')


@login_required(login_url='/reg/')
def paymentb(request):
    if request.method == "POST":
        try:
            ps = request.POST['paymentSystem']
            ticket = request.POST['ticket']
            t = Ticket.objects.get(id=ticket)
            i = Invoiceb()
            i.user = request.user
            i.status = 0
            i.type = 2
            b = Bank.objects.get(id=ps)
            i.system = b
            i.ticket = t
            t.status = 1
            t.save()
            i.price = t.price
            i.save()
            m = Message()
            m.user = request.user
            m.read = False
            m.title = _(u'Заявка на оплату принята')
            m.text = _(u'Ваша заявка была принята на рассмотрение ознакомится с ней вы можете по ссылке'
                       u' <a href="http://euro-paradise.com/cp/invoiceb/{0}/">Счет номер {1}</a>'.format(i.id, i.id))
            m.save()
            return HttpResponse(i.id)
        except:
            return HttpResponse('err')


@login_required(login_url='/reg/')
def admin(request):
    context = {'title': _(u'| Панель администратора')}
    return render(request, 'cp/admin.html', context)


@login_required(login_url='/reg/')
def invoicep(request, id):
    i = get_object_or_404(Invoicep, id=id)
    if i.user == request.user:
        context = {'i': i}
        return render(request, 'cp/invoicep.html', context)
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


def invoiceb(request, id):
    i = get_object_or_404(Invoiceb, id=id)
    if i.user == request.user:
        context = {'i': i}
        return render(request, 'cp/invoiceb.html', context)
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def invoicepticket(request, id):
    t = Ticket.objects.get(user=request.user, id=id)
    i = get_object_or_404(Invoicep, ticket=t)
    if i.user == request.user:
        context = {'i': i}
        return render(request, 'cp/invoicep.html', context)
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def invoicebticket(request, id):
    t = Ticket.objects.get(user=request.user, id=id)
    i = get_object_or_404(Invoiceb, ticket=t)
    if i.user == request.user:
        context = {'i': i}
        return render(request, 'cp/invoiceb.html', context)
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def invoicebonusticket(request, id):
    t = Ticket.objects.get(user=request.user, id=id)
    i = get_object_or_404(Invoicebonus, ticket=t)
    if i.user == request.user:
        context = {'i': i}
        return render(request, 'cp/invoicebonus.html', context)
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def cancelticket(request):
    if request.method == "POST":
        id = request.POST['ticket']
        try:
            t = Ticket.objects.get(user=request.user, id=id)
            t.delete()
            g = t.game
            g.ticketsa = g.ticketsa + 1
            g.save()
            return HttpResponse('1')
        except:
            return HttpResponse('0')


@login_required(login_url='/reg/')
def getmsg(request, id):
    m = get_object_or_404(Message, id=id)
    if m.user == request.user:
        m.read = True
        m.save()
        jdict = []
        jdict.append({"date": str(m.date), "title": m.title, "text": m.text, "id": m.id})
        return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")
    else:
        request.session['alert'] = _(u'У вас нет доступа.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def rmmsg(request):
    if request.method == "POST":
        idsl = request.POST['idMsg'].split(',')
        for item in idsl:
            m = get_object_or_404(Message, id=item)
            if m.user == request.user:
                m.delete()
            else:
                request.session['alert'] = _(u'У вас нет доступа.')
                return HttpResponseRedirect('/cp/')
        request.session['alert'] = _(u'Удаленно успешно')
        return HttpResponse('1')


@login_required(login_url='/reg/')
def unreadmsg(request):
    if request.method == "POST":
        idsl = request.POST['idMsg'].split(',')
        for item in idsl:
            m = get_object_or_404(Message, id=item)
            if m.user == request.user:
                m.read = False
                m.save()
            else:
                request.session['alert'] = _(u'У вас нет доступа.')
                return HttpResponseRedirect('/cp/')
        return HttpResponse('1')


@login_required(login_url='/reg/')
def readmsg(request):
    if request.method == "POST":
        idsl = request.POST['idMsg'].split(',')
        for item in idsl:
            m = get_object_or_404(Message, id=item)
            if m.user == request.user:
                m.read = True
                m.save()
            else:
                request.session['alert'] = _(u'У вас нет доступа.')
                return HttpResponseRedirect('/cp/')
        return HttpResponse('1')


@login_required(login_url='/reg/')
def changepass(request):
    if request.method == "POST":
        p = request.POST['pass']
        u = request.user
        u.set_password(p)
        u.save()
        request.session['alert'] = _(u'Пароль успешно изменен.')
        return HttpResponseRedirect('/cp/')


@login_required(login_url='/reg/')
def inner(request):
    if request.method == "POST":
        p = int(request.POST['pointsamount'])
        try:
            l = User.objects.get(username=request.POST['login'])
        except:
            request.session['alert'] = _(u'Пользователь назначения не найден.')
            return HttpResponseRedirect('/cp/payments/')
        ui = request.user.partners
        if ui.bal < p:
            request.session['alert'] = _(u'Недостаточно бонусов на счету.')
            return HttpResponseRedirect('/cp/payments/')
        ui.bal = ui.bal - p
        ui.save()
        lui = l.partners
        lui.bal = lui.bal + p
        lui.save()
        request.session['alert'] = _(u'Платеж прошел успешно. Бонусы списаны с вашего счета')
        return HttpResponseRedirect('/cp/payments/')


@login_required(login_url='/reg/')
def cashtrans(request):
    if request.method == "POST":
        p = int(request.POST['points'])
        n = request.POST['name']
        a = request.POST['address']
        numb = request.POST['bankacc']
        bn = request.POST['bankname']
        ba = request.POST['bankaddress']
        sw = request.POST['swift']
        ib = request.POST['iban']
        u = request.user
        ui = u.partners
        if ui.bal < p:
            request.session['alert'] = _(u'Недостаточно бонусов на счету.')
            return HttpResponseRedirect('/cp/payments/')
        ui.bal = ui.bal - p
        ui.save()
        w = Wreq()
        w.user = u
        w.status = 0
        w.points = p
        w.name = n
        w.address = a
        w.bankname = bn
        w.bankaddress = ba
        w.number = numb
        w.iban = ib
        w.swift = sw
        w.save()
        request.session['alert'] = _(u'Ваша заявка принята на рассмотрение!'
                                     u' В самое ближайшее время вы получите уведомление.')
        return HttpResponseRedirect('/cp/payments/')
