from django import template
from cp.models import Message

register = template.Library()

@register.assignment_tag
def mcount(user):
    mc = Message.objects.filter(user=user, read=False).count()
    return mc

