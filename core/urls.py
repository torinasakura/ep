from django.conf.urls import patterns, include, url
from django.contrib import admin
from sitemap import Statics

sitemaps = {'Static': Statics}

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'index.views.index', name='index'),
                       url(r'^bonus/', 'index.views.bonus', name='bonus'),
                       url(r'^phones/', 'index.views.phones', name='phones'),
                       url(r'^moneys/', 'index.views.moneys', name='moneys'),
                       url(r'^travels/', 'index.views.travels', name='travels'),
                       url(r'^autos/', 'index.views.autos', name='autos'),
                       url(r'^penthouses/', 'index.views.penthouses', name='penthouses'),
                       url(r'^how/', 'index.views.how', name='how'),
                       url(r'^rules/', 'index.views.rules'),
                       url(r'^online/', 'index.views.online', name='online'),
                       url(r'^archives/', 'index.views.archives'),
                       url(r'^about/', 'index.views.about'),
                       url(r'^impresum/', 'index.views.impresum'),
                       url(r'^contacts/', 'index.views.contacts'),
                       url(r'^reg/', 'index.views.reg', name='reg'),
                       url(r'^fpass/', 'index.views.fpass'),
                       url(r'^snpass/(?P<key>\w+)/', 'index.views.snpass'),
                       url(r'^login/', 'index.views.loginv'),
                       url(r'^logout/', 'index.views.logoutv'),
                       url(r'^checkticket/', 'index.views.checkticket'),
                       url(r'^ftickets/(?P<id>\d+)/', 'index.views.ftickets'),
                       url(r'^checkgame/(?P<id>\d+)/', 'index.views.checkgame'),
                       url(r'^buyticket/', 'index.views.buyticket'),
                       url(r'^emailv/(?P<key>\w+)/', 'index.views.email_verification'),
                       url(r'^gettickets/(?P<id>\d+)/', 'index.views.gettickets'),
                       url(r'^ref/(?P<id>\d+)/', 'index.views.ref'),
                       #CP
                       url(r'^cp/', include('cp.urls')),
                       #Admin UI
                       url(r'^adminui/', include('adminui.urls')),
                       #Admin
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^admin/', include(admin.site.urls)),

                       url(r'^i18n/', include('django.conf.urls.i18n')),
                       (r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
)

js_info_dict = {
    'packages': ('core',),
}

urlpatterns += patterns('',
                        (r'^jsi18n/$', 'django.views.i18n.javascript_catalog', js_info_dict),
)
