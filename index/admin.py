from django.contrib import admin
from index.models import Series, Game, Ticket, Gtype, State, Country
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User


admin.site.register(Series)
admin.site.register(Game)
admin.site.register(Ticket)
admin.site.register(Gtype)
admin.site.register(State)
admin.site.register(Country)

UserAdmin.list_display = ('email', 'first_name', 'last_name', 'date_joined', 'last_login', 'is_active', 'is_staff')

admin.site.unregister(User)
admin.site.register(User, UserAdmin)