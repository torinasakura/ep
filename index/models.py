from django.db import models
from django.contrib.auth.models import User


class Gtype(models.Model):
    name = models.CharField(max_length=300, verbose_name=u'Имя игры')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Тип игры"
        verbose_name_plural = u"Типы игр"


class Series(models.Model):
    type = models.ForeignKey(Gtype, verbose_name=u'Тип игры')
    active = models.BooleanField(verbose_name=u'Активно')
    startdate = models.DateField(auto_now_add=True, verbose_name=u'Дата начала')
    stopdate = models.DateField(verbose_name=u'Дата окончания', blank=True, null=True)

    def __unicode__(self):
        return self.type.name + u' - начало ' + str(self.startdate) + u' - конец ' + str(self.stopdate)

    class Meta:
        verbose_name = u"Серия"
        verbose_name_plural = u"Серии"


class Game(models.Model):
    status = models.IntegerField(default=0, verbose_name=u'Статус')
    series = models.ForeignKey(Series, verbose_name=u'Серия')
    num = models.IntegerField(default=0, verbose_name=u'Номер')
    tickets = models.IntegerField(default=0, verbose_name=u'Билетов')
    ticketsa = models.IntegerField(default=0, verbose_name=u'Билетов доступно')
    price = models.IntegerField(default=0, verbose_name=u'Цена')
    startdate = models.DateField(auto_now_add=True, verbose_name=u'Дата начала')
    gamedate = models.DateField(verbose_name=u'Дата проведения', blank=True, null=True)
    stopdate = models.DateField(verbose_name=u'Дата окончания', blank=True, null=True)

    def __unicode__(self):
        return self.series.type.name + ' ' + str(self.num)

    class Meta:
        verbose_name = u"Игра"
        verbose_name_plural = u"Игры"


class Ticket(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    number = models.IntegerField(default=0, verbose_name=u'Номер')
    numbert = models.CharField(max_length=20, verbose_name=u'Номер')
    price = models.IntegerField(default=0, verbose_name=u'Цена')
    status = models.IntegerField(default=0, verbose_name=u'Статус')
    game = models.ForeignKey(Game, verbose_name=u'Игра')
    bdate = models.DateTimeField(verbose_name=u'Дата брони')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u"Билет"
        verbose_name_plural = u"Билеты"


class Country(models.Model):
    name = models.CharField(max_length=300, verbose_name=u'Имя')
    code = models.CharField(max_length=20, verbose_name=u'Код телефона')
    pcode = models.CharField(max_length=20, verbose_name=u'Код страны')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Страна"
        verbose_name_plural = u"Страны"


class State(models.Model):
    count = models.ForeignKey(Country, verbose_name=u'Страна')
    name = models.CharField(max_length=100, verbose_name=u'Имя')
    code = models.CharField(max_length=100, verbose_name=u'Код')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Область"
        verbose_name_plural = u"Области"