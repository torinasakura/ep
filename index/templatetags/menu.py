from django import template

register = template.Library()

@register.inclusion_tag('tt/menu.html')
def menu(path, user):
    return {'path': path,
            'user': user}

@register.inclusion_tag('tt/cpmenu.html')
def cpmenu(path, user):
    return {'path': path,
            'user': user}

@register.inclusion_tag('tt/adminuimenu.html')
def adminuimenu(path):
    return {'path': path}