from django import template

register = template.Library()

@register.inclusion_tag('tt/alert.html')
def alert(session):
    if session['alert']:
        alert = session['alert']
        del session['alert']
        return {'alert' : alert}
