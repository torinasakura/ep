﻿import json
import hashlib
import django.core.mail
import md5
from datetime import datetime, timedelta
from django.shortcuts import render, redirect, get_object_or_404
from models import Game, Ticket, Series, Country, State
from cp.models import Userinfo, Emailv, Rpass, Bonus, Partners, Profit
from decimal import Decimal
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import send_mail
from django.contrib.auth import logout, authenticate, login
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from datetime import datetime
from adminui.models import Bank, Purse
from django.utils.translation import ugettext_lazy as _


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


def gettickets(request, id):
    if request.user.is_authenticated():
        s = Series.objects.get(type=id, active=True)
        game = s.game_set.order_by('num')
        tickets = Ticket.objects.all().filter(user=request.user, game__in=game)
        jdict = []
        for item in tickets:
            jdict.append({"id": item.id, "num": item.number, "gameNum": item.game.num,
                          "price": item.price, "status": item.status})
        return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")
    else:
        return HttpResponse('err')


def index(request):
    context = {'title': _(u'| Главная')}
    return render(request, 'index.html', context)


def bonus(request):
    context = {'title': _(u'| Бонусная программа')}
    return render(request, 'bonus.html', context)


def phones(request):
    s = Series.objects.get(type=1, active=True)
    game = s.game_set.order_by('num')
    context = {
        'game': game,
        'title': _(u'| Розыгрыш телефонов')}
    b = Bank.objects.all()
    p = Purse.objects.all()
    context['banks'] = b
    context['purses'] = p
    if request.user.is_authenticated():
        tickets = Ticket.objects.all().filter(user=request.user, game__in=game)
        context['tickets'] = tickets
    return render(request, 'phones.html', context)


def moneys(request):
    s = Series.objects.get(type=2, active=True)
    game = s.game_set.order_by('num')
    context = {
        'game': game,
        'title': _(u'| Розыгрыш денег')}
    b = Bank.objects.all()
    p = Purse.objects.all()
    context['banks'] = b
    context['purses'] = p
    if request.user.is_authenticated():
        tickets = Ticket.objects.all().filter(user=request.user, game__in=game)
        context['tickets'] = tickets
    return render(request, 'moneys.html', context)


def travels(request):
    s = Series.objects.get(type=3, active=True)
    game = s.game_set.order_by('num')
    context = {
        'game': game,
        'title': _(u'| Розыгрыш путешествий')}
    b = Bank.objects.all()
    p = Purse.objects.all()
    context['banks'] = b
    context['purses'] = p
    if request.user.is_authenticated():
        tickets = Ticket.objects.all().filter(user=request.user, game__in=game)
        context['tickets'] = tickets
    return render(request, 'travels.html', context)


def autos(request):
    s = Series.objects.get(type=4, active=True)
    game = s.game_set.order_by('num')
    context = {
        'game': game,
        'title': _(u'| Розыгрыш автомобилей')}
    b = Bank.objects.all()
    p = Purse.objects.all()
    context['banks'] = b
    context['purses'] = p
    if request.user.is_authenticated():
        tickets = Ticket.objects.all().filter(user=request.user, game__in=game)
        context['tickets'] = tickets
    return render(request, 'autos.html', context)


def penthouses(request):
    s = Series.objects.get(type=5, active=True)
    game = s.game_set.order_by('num')
    context = {
        'game': game,
        'title': _(u'| Розыгрыш пентхаусов')}
    b = Bank.objects.all()
    p = Purse.objects.all()
    context['banks'] = b
    context['purses'] = p
    if request.user.is_authenticated():
        tickets = Ticket.objects.all().filter(user=request.user, game__in=game)
        context['tickets'] = tickets
    return render(request, 'penthouses.html', context)


def how(request):
    context = {'title': _(u'| Как начать играть')}
    return render(request, 'how.html', context)


def rules(request):
    context = {'title': _(u'| Правила игры')}
    return render(request, 'rules.html', context)


def online(request):
    context = {'title': _(u'| Прямая трансляция')}
    return render(request, 'online.html', context)


def archives(request):
    tickets = Ticket.objects.all().filter(status=3)
    context = {'title': _(u'| Архив розыгрышей'),
               'tickets': tickets}
    return render(request, 'archives.html', context)


def about(request):
    context = {'title': _(u'| О компании')}
    return render(request, 'about.html', context)


def impresum(request):
    context = {'title': _(u'| Impresum')}
    return render(request, 'impresum.html', context)


def contacts(request):
    context = {'title': _(u'| Контакты')}
    if request.method == "POST":
        name = request.POST['name'].encode('utf-8')
        email = request.POST['email'].encode('utf-8')
        message = request.POST['message'].encode('utf-8')
        text = 'Сообщение от: ' + name + '\n\n Email: ' + email + '\n\n Текст сообщения: \n' + message
        title = 'Быстрое сообщение с сайта от ' + name + ' ' + message
        if send_mail(title, text, 'ta@cyberthrone.org', ['programmeraxel@gmail.com'], fail_silently=True):
            request.session['alert'] = _(u'Сообщение успешно отправленно')
        else:
            request.session['alert'] = _(u'В процессе отправки возникла ошибка')
    return render(request, 'contacts.html', context)


def cp(request):
    context = {'title': _(u'| Личный кабинет')}
    return render(request, 'cp.html', context)


def fpass(request):
    context = {'title': _(u' Восстановление пароля ')}
    if request.method == "POST":
        username = request.POST['login']
        try:
            user = User.objects.get(username=username)
            rpass = Rpass.objects.get_or_create(user=user)[0]
            key = md5.md5(username+'2rwefs345duhn').hexdigest()
            rpass.key = key
            rpass.save()
            text = _(u'Для изменения пароля перейдите по ссылке:\n\n'
                     u'http://euro-paradise.com/snpass/%s/\n\n'
                     u'Игнориуйте данное сообщение если вы не востанавливали пароль') % key
            send_mail(_(u'Смена пароля euro-paradise.com'), text, 'ta@cyberthrone.org',
                      [user.email], fail_silently=True)
            request.session['alert'] = _(u'Инструкции по смене пароля высланы на ваш Email')
            return HttpResponseRedirect(reverse('index.views.index'))
        except user.DoesNotExist:
            request.session['alert'] = _(u'Данный пользователь не зарегестрирован')
            return render(request, 'fpass.html', context)
    return render(request, 'fpass.html', context)


def snpass(request, key):
    context = {'title': _(u' Смена пароля ')}
    if request.method == "POST":
        newpass = request.POST['pass']
        newpass1 = request.POST['pass1']
        if newpass == newpass1:
            rpass = get_object_or_404(Rpass, key=key)
            user = rpass.user
            user.set_password(newpass)
            user.save()
            rpass.delete()
            request.session['alert'] = _(u'Пароль успешно изменен.')
            return HttpResponseRedirect(reverse('index.views.index'))
        else:
            request.session['alert'] = _(u'Пароли не совпадают')
            return render(request, 'snpass.html', context)
    get_object_or_404(Rpass, key=key)
    return render(request, 'snpass.html', context)


def email_verification(request, key):
    mkey = get_object_or_404(Emailv, key=key)
    user = mkey.user
    user.is_active = True
    user.save()
    mkey.delete()
    request.session['alert'] = _(u'Вы успешно подтвердили email теперь можете авторизоватся.')
    return HttpResponseRedirect(reverse('index.views.reg'))


def reg(request):
    context = {'title': _(u'| Регистрация')}
    if request.method == "POST":
        #try:
        login = request.POST['login']
        name = request.POST['name']
        surname = request.POST['surname']
        country = request.POST['country']
        city = request.POST['city']
        phone = request.POST['phone']
        email = request.POST['email']
        password = request.POST['password']
        agree = request.POST['agree']
        bday = datetime.strptime(request.POST['birthday'], '%d/%m/%Y')
        if agree == 'on':
            user, created = User.objects.get_or_create(username=login)
            if created:
                user.set_password(password)
                user.email = email
            else:
                request.session['alert'] = _(u'Данный логин уже используется')
                return HttpResponseRedirect(reverse('index.views.reg'))
            user.first_name = name
            user.last_name = surname
            user.is_active = False
            key = md5.md5(login + 'sol234t1').hexdigest()
            mailkey = Emailv.objects.get_or_create(user=user)[0]
            mailkey.key = key
            mailkey.save()
            user.save()
            if "ref" in request.COOKIES:
                u5 = user
                u4 = User.objects.get(id=request.COOKIES["ref"])
                b1 = Bonus()
                b1.user = u5
                b1.refer = u4
                b1.save()
                p = Profit()
                p.user = u5
                p.refer = u4
                p.save()
                u4p = u4.partners
                u4p.countp = u4p.countp + 1
                u4p.save()
                bonus = Bonus.objects.filter(user=u4)
                if bonus.count() > 0:
                    u3 = bonus[0].refer
                    p = Profit()
                    p.user = u5
                    p.refer = u3
                    p.save()
                    u3p = u3.partners
                    u3p.countp = u3p.countp + 1
                    u3p.save()
                    bonus = Bonus.objects.filter(user=u3)
                    if bonus.count() > 0:
                        u2 = bonus[0].refer
                        p = Profit()
                        p.user = u5
                        p.refer = u2
                        p.save()
                        u2p = u2.partners
                        u2p.countp = u2p.countp + 1
                        u2p.save()
                        bonus = Bonus.objects.filter(user=u2)
                        if bonus.count() > 0:
                            u1 = bonus[0].refer
                            p = Profit()
                            p.user = u5
                            p.refer = u1
                            p.save()
                            u1p = u1.partners
                            u1p.countp = u1p.countp + 1
                            u1p.save()
            else:
                refer = User.objects.get(id=1)
                b = Bonus()
                b.user = user
                b.refer = refer
                b.save()
                p = Profit()
                p.user = user
                p.refer = refer
                p.save()
                referp = refer.partners
                referp.countp = referp.countp + 1
                referp.save()
            p = Partners()
            p.user = user
            p.save()
            userinfo = Userinfo()
            userinfo.user = user
            c = Country.objects.get(pcode=country)
            userinfo.country = c
            s = State.objects.get(code=city, count=c)
            userinfo.city = s
            userinfo.bday = bday
            userinfo.phone = phone
            try:
                skype = request.POST['skype']
                userinfo.skype = skype
            except:
                print 'user do not use skype'
            userinfo.save()
            text = _(u'Добро пожаловать на портал Euro Paradise!\n\n'
                     u'Ваша учётная запись ещё не активна. Вы не сможете ей пользоваться,'
                     u' пока не перейдёте по следующей ссылке:\n\n'
                     u'http://euro-paradise.com/emailv/%s/\n\nСпасибо за то, что зарегистрировались!') % key
            send_mail(_(u'подтверждение Email euro-paradise.com'), text, 'ta@cyberthrone.org',
                      [email], fail_silently=True)
            request.session['alert'] = _(u'Вы успешно зарегестрировались, инструкции высланы на ваш Email')
        return HttpResponseRedirect(reverse('index.views.reg'))
        #except:
        #    request.session['alert'] = _(u'Возникла непредвиденная ошибка')
        #    return HttpResponseRedirect(reverse('index.views.reg'))
    return render(request, 'reg.html', context)


def loginv(request):
    if request.method == "POST":
        username = request.POST['login']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                try:
                    if request.POST['next']:
                        return redirect(request.POST['next'])
                except:
                    return HttpResponseRedirect(reverse('index.views.index'))
                return HttpResponseRedirect(reverse('index.views.index'))
            else:
                request.session['alert'] = _(u'Ваш аккаунт не активен')
                return HttpResponseRedirect(reverse('index.views.reg'))
        else:
            request.session['alert'] = _(u'Неверный логин или пароль')
            return HttpResponseRedirect(reverse('index.views.reg'))
    else:
        request.session['alert'] = _(u'При выполнении запроса возникла ошибка')
        return HttpResponseRedirect(reverse('index.views.reg'))


def logoutv(request):
    logout(request)
    request.session['alert'] = _(u'Вы вышли с сайта')
    return HttpResponseRedirect(reverse('index.views.index'))


def checkticket(request):
    game = request.GET['game']
    ticket = request.GET['ticket']
    t = Ticket.objects.filter(number=ticket, game=game)
    if t.count() > 0:
        return HttpResponse(0)
    else:
        return HttpResponse(1)


@login_required(login_url='/reg/')
def buyticket(request):
    if request.method == "POST":
        game = request.POST['game']
        ticket = request.POST['ticket']
        g = get_object_or_404(Game, id=game)
        u = request.user
        try:
            t = Ticket.objects.get(number=ticket, game=g)
            return HttpResponse('0')
        except ObjectDoesNotExist:
            t = Ticket()
            t.user = u
            t.price = g.price
            t.status = 0
            t.number = ticket
            t.numbert = ticket
            t.bdate = datetime.now() + timedelta(days=3)
            t.game = g
            g.ticketsa = g.ticketsa - 1
            g.save()
            t.save()
            return HttpResponse('{"idTicket":"' + str(t.id) + '","numTicket":"' + str(t.number) +
                                '","numGame":"' + str(g.id) + '","typeGame":"' + str(g.series.type) +
                                '","price":"' + str(g.price) + '","date":"' + str(t.bdate) + '"}')
    return HttpResponse('err')


def checkgame(request, id):
    g = get_object_or_404(Game, id=id)
    return HttpResponse('{"status":"' + str(g.status) + '","ticketsa":"' + str(g.ticketsa) + '"}')


def ftickets(request, id):
    g = get_object_or_404(Game, id=id)
    i = 0
    l = []
    while i <= g.tickets:
        t = Ticket.objects.filter(number=i, game=g.id)
        if t.count() == 0:
            l.append(i)
        i = i + 1
    k = '['
    for item in l:
        k = k+str(item) + ','
    k = k[:-1] + ']'
    return HttpResponse(k)


def fm(request):
    if request.method == "POST":
        e = request.POST['email'].encode('utf-8')
        m = request.POST['message'].encode('utf-8')
        n = request.POST['name'].encode('utf-8')
        text = 'Сообщение от: ' + n + '\n\n Email: ' + e + '\n\n Текст сообщения: \n' + m
        title = 'Быстрое сообщение с сайта от ' + n + ' ' + m
        return HttpResponse(send_mail(title, text, 'me@torinasakura.name',
                                      ['programmeraxel@gmail.com'], fail_silently=True))


def ref(request, id):
    response = HttpResponseRedirect('/')
    response.set_cookie('ref', id, 12960000)
    return response