from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse


class Statics(Sitemap):
    changefreq = 'monthly'

    def priority(self, item):
        return item[1]

    def items(self):
        return [['penthouses', 1], ['autos', 0.99], ['travels', 0.98], ['moneys', 0.97], ['phones', 0.96],
                ['reg', 0.95], ['how', 0.94], ['bonus', 0.93], ['online', 0.92], ['index', 0.9]]

    def location(self, item):
        return reverse(item[0])